# Msmtp RPM

This is a RPM specification file used to build RPMs for msmtp.


---
**There's no need to build the package yourself**, if you're using CentOS/RedHat >= 7. You can find ready to use RPMs on Fedora COPR:

https://copr.fedorainfracloud.org/coprs/leisteth/msmtp

---


## Build package on Fedora using mock

Make sure you have rpmbuild and mock installed:

```bash
sudo dnf install rpmbuild mock
```

Download all sources defined in SPEC file:

```bash
spectool -g -R yggdrasil.spec
```

Create a SRPM (source RPM) package from the SPEC file:

```bash
rpmbuild -bs yggdrasil.spec
```

Create RPM package in an isolated environment using `mock`:

```bash
mock -r fedora-30-x86_64 --rebuild ~/rpmbuild/SRPMS/yggdrasil-0.3.5-1.fc30.src.rpm --old-chroot
```

*(--old-chroot is needed because it enables internet connection in the build environment)*

You will find your package in `/var/lib/mock/fedora-29-x86_64/result`.
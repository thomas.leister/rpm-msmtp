Name:           msmtp
Version:        1.8.5
Release:        1%{?dist}
Summary:        msmtp is a lightweigt SMTP client

License:        GPLv3
URL:            https://marlam.de/msmtp/
Source:         https://marlam.de/msmtp/releases/msmtp-%{version}.tar.xz

%{?systemd_requires}
BuildRequires:  autoconf automake texinfo gettext-devel gcc gnutls-devel libidn-devel systemd
Requires:       msmtp systemd
Conflicts:      postfix exim


%description
msmtp is a sendmail compatible SMTP client. 
This package adds a symlink from /usr/sbin/sendmail to msmtp to enable it for system mail sending,
e.g. when running cron jobs.


%prep
%setup -qn msmtp-%{version}

%build
autoreconf -i
%configure
make


%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sbindir}
install -m 0755 src/msmtp %{buildroot}/%{_bindir}/msmtp
install -m 0750 src/msmtpd %{buildroot}%{_sbindir}/msmtpd

%files
%{_bindir}/msmtp
%{_sbindir}/msmtpd


%post
ln -s -v %{_bindir}/msmtp %{_sbindir}/sendmail
ln -s -v %{_bindir}/msmtp %{_libdir}/sendmail


%postun
rm -v %{_sbindir}/sendmail
rm -v %{_libdir}/sendmail

